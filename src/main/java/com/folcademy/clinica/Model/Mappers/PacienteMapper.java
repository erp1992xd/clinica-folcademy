package com.folcademy.clinica.Model.Mappers;


import com.folcademy.clinica.Model.Dtos.*;

import com.folcademy.clinica.Model.Entities.*;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Optional;

@Component
public class PacienteMapper {
    private final PersonaMapper personaMapper = new PersonaMapper();

    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getId(),
                                ent.getDni(),
                                ent.getDomicilio()
                        )
                )
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente();
        entity.setId(dto.getId());
        entity.setDni(dto.getDni());
        entity.setDomicilio(dto.getDomicilio());
        return entity;
    }
    public PacienteEnteroDto entityToEnteroDto (Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteEnteroDto(
                                ent.getId(),
                                ent.getPersona(),
                                ent.getDomicilio()
                        )
                )
                .orElse(new PacienteEnteroDto());
    }

    public Paciente enteroDtoToEntity(PacienteEnteroDto dto){
        Paciente entity = new Paciente();
        entity.setId(dto.getId());
        entity.setPersona((Persona)dto.getPersona());
        entity.setDomicilio(dto.getDomicilio());
        return entity;
    }

}