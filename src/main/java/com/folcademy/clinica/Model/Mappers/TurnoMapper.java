package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;
import java.util.Optional;

@Component
public class TurnoMapper {
    public TurnoDto entityToDto(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdmedico(),
                                ent.getIdpaciente()

                        )
                )
                .orElse(new TurnoDto());
    }

    public Turno dtoToEntity(TurnoDto dto) {
        Turno entity = new Turno();
        entity.setId(dto.getId());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdmedico(dto.getIdmedico());
        entity.setIdpaciente(dto.getIdpaciente());

        return entity;
    }

    public Turno enteroDtoToEntity(TurnoEnteroDto dto) {
        Turno entity = new Turno();
        entity.setId(dto.getId());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setMedico((Medico) dto.getMedico());
        entity.setPaciente((Paciente)dto.getPaciente());

        return entity;
    }

    public TurnoEnteroDto entityToEnteroDto(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoEnteroDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getMedico(),
                                ent.getPaciente()
                        )
                )
                .orElse(new TurnoEnteroDto());
    }
}