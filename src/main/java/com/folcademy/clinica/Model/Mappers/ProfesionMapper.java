package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.ProfesionDto;
import com.folcademy.clinica.Model.Dtos.ProfesionEnteroDto;

import com.folcademy.clinica.Model.Entities.Profesion;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ProfesionMapper {
    public ProfesionDto entityToDto(Profesion entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new ProfesionDto(
                                ent.getId(),
                                ent.getNombre()

                        )
                )
                .orElse(new ProfesionDto());
    }

    public Profesion dtoToEntity(ProfesionDto dto){
        Profesion entity = new Profesion();
        entity.setId(dto.getId());
        entity.setNombre(dto.getNombre());
        return entity;
    }

    public ProfesionEnteroDto entityToEnteroDto (Profesion entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new ProfesionEnteroDto(
                                ent.getId(),
                                ent.getNombre()

                        )
                )
                .orElse(new ProfesionEnteroDto());
    }

    public Profesion enteroDtoToEntity(ProfesionEnteroDto dto){
        Profesion entity = new Profesion();
        entity.setId(dto.getId());
        entity.setNombre(dto.getNombre());
        return entity;
    }

}