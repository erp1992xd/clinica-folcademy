package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Entities.Profesion;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Optional;

@Component
public class MedicoMapper {
    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                        ent.getId(),
                        ent.getDni(),
                        ent.getIdprofesion(),
                        ent.getConsulta()

                        )
                )
                .orElse(new MedicoDto());
    }

    public Medico dtoToEntity(MedicoDto dto){
        Medico entity = new Medico();
        entity.setId(dto.getId());
        entity.setDni(dto.getDni());
        entity.setIdprofesion(dto.getIdprofesion());
        entity.setConsulta(dto.getConsulta());
        return entity;
    }

    public MedicoEnteroDto entityToEnteroDto (Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoEnteroDto(
                                ent.getId(),
                                ent.getPersona(),
                                ent.getProfesion(),
                                ent.getConsulta()

                        )
                )
                .orElse(new MedicoEnteroDto());
    }

    public Medico enteroDtoToEntity(MedicoEnteroDto dto){
        Medico entity = new Medico();
        entity.setId(dto.getId());
        entity.setPersona((Persona)dto.getPersona());
        entity.setProfesion((Profesion)dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        return entity;
    }

}
