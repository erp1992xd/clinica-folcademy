package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Entities.Profesion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("profesionRepository")
public interface ProfesionRepository extends PagingAndSortingRepository<Profesion, Integer> {
    Page<Profesion> findAll(Pageable pageable);
    Page<Profesion> findById(Integer id, Pageable pageable);


}
