package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.GeneratedValue;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class MedicoDto {

    Integer id;
    Integer dni;
    Integer idprofesion;
    Integer consulta;

}
