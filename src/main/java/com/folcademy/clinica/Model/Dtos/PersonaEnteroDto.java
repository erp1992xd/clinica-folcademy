package com.folcademy.clinica.Model.Dtos;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.GeneratedValue;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class PersonaEnteroDto extends PersonaDto {

    Integer dni;
    @NotNull
    String nombre;
    @NotNull
    String apellido;
    @NotNull
    String telefono;


}