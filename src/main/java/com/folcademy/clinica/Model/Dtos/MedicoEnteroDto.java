package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Entities.Profesion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoEnteroDto {
    Integer id;
    Persona persona;
    Profesion profesion;
    Integer consulta;

}
