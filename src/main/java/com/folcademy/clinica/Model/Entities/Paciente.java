package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.PersonaDto;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "paciente")
@Getter
@Setter
@ToString
@RequiredArgsConstructor

public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;
    @Column(name = "Dni", columnDefinition = "INT")
    public Integer dni;
    @Column(name = "Domicilio", columnDefinition = "VARCHAR")
    String domicilio = "";


    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "dni",referencedColumnName = "dni", insertable = false, updatable = false)
    private  Persona persona;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Paciente paciente = (Paciente) o;
        return id != null && Objects.equals(id, paciente.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
