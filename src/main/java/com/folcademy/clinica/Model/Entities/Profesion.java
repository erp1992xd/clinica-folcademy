package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.ProfesionDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "profesion")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Profesion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idprofesion", columnDefinition = "INT(10) UNSIGNED")
    Integer id;
    @Column(name = "nombre", columnDefinition = "VARCHAR")
    public String nombre = "";


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Profesion profesion = (Profesion) o;
        return id != null && Objects.equals(id, profesion.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
