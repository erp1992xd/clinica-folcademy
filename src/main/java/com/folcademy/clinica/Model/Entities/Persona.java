package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "persona")
@Getter
@Setter
@ToString
@RequiredArgsConstructor


public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dni", columnDefinition = "INT")
    public Integer dni;
    @Column(name = "Nombre", columnDefinition = "VARCHAR")
    String nombre ;
    @Column(name = "Apellido", columnDefinition = "VARCHAR")
    String apellido;
    @Column(name = "Telefono", columnDefinition = "VARCHAR")
    String telefono;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Persona persona = (Persona) o;
        return dni != null && Objects.equals(dni, persona.dni);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}