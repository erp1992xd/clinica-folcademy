package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor


public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    Integer id;
    @Column(name = "Dni", columnDefinition = "INT")
    Integer dni;
    @Column(name = "Idprofesion", columnDefinition = "VARCHAR")
    Integer idprofesion;
    @Column(name = "Consulta", columnDefinition = "VARCHAR")
    Integer consulta;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "dni",referencedColumnName = "dni", insertable = false, updatable = false)
    private  Persona persona;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idprofesion",referencedColumnName = "idprofesion", insertable = false, updatable = false)
    private  Profesion profesion;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;
        return id != null && Objects.equals(id, medico.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
