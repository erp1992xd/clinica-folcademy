package com.folcademy.clinica.Services;


import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.OnlyLettersException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicoService /*implements IMedicoService*/ {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper){
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public List<Medico> findAllMedico() {
        return (List<Medico>) medicoRepository.findAll();
    }

    public List<Medico> findMedicoById(Integer id) {
        List<Medico> lista = new ArrayList<>();
        if (!medicoRepository.existsById(id))
            throw new NotFoundException("Medico no encontrado");
        Medico medico = medicoRepository.findById(id).get();
        lista.add(medico);
        return lista;
    }

    public MedicoDto listarUno(Integer idmedico) {
        return medicoRepository.findById(idmedico).map(medicoMapper::entityToDto).orElse(null);
    }

    static boolean esSoloLetras(String cadena) {
        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.toUpperCase().charAt(i);
            int valorASCII = (int) caracter;
            if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
                return false;
        }
        return true;
    }


    public static boolean esSoloNumeros(String cadena) {
        return cadena.matches("[0-9]+");
    }


    public MedicoDto save (MedicoDto entity){
        entity.setId(null);
        if(entity.getConsulta()<=0){
            throw new BadRequestException("La consulta no puede ser menor a 0");
        }
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));

    }

    public MedicoEnteroDto editar (Integer id,MedicoEnteroDto dto){
        if (!medicoRepository.existsById(id))
            //return null;
            throw new NotFoundException("No existe el medico");
        dto.setId(id);
        //dto -> entidad;
        //guardar;
        //entidad -> dto;
        return
                medicoMapper.entityToEnteroDto(
                        medicoRepository.save(
                                medicoMapper.enteroDtoToEntity(dto)
                        )
                );
    }

    public boolean eliminar(Integer id) {

        if (!medicoRepository.existsById(id))
            throw new NotFoundException("Medico no encontrado");

        medicoRepository.deleteById(id);
        return true;
    }

    /*public List<MedicoDto> listarTodos(){
        List<Medico> medicos = (List<Medico>) medicoRepository.findAll();
        return medicos.stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }*/

    public Page<MedicoEnteroDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToEnteroDto);
    }
    public Page<MedicoEnteroDto> listarUnoByPage(Integer id) {
        Pageable pageable = PageRequest.of(0,1,Sort.by("id"));
        return medicoRepository.findById(id,pageable).map(medicoMapper::entityToEnteroDto);
    }

}
