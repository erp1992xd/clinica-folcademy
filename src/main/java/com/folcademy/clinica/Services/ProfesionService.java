package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.*;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Profesion;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.ProfesionMapper;
import com.folcademy.clinica.Model.Repositories.ProfesionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfesionService {

    private final ProfesionRepository profesionRepository;
    private final ProfesionMapper profesionMapper;

    public ProfesionService(ProfesionRepository profesionRepository, ProfesionMapper profesionMapper) {
        this.profesionRepository = profesionRepository;
        this.profesionMapper = profesionMapper;
    }

    /*public List<Profesion> findAllProfesiones() {
        if (profesionRepository.findAll().isEmpty())
            throw new NotFoundException("No hay turnos registrados");
        return (List<Profesion>) profesionRepository.findAll();
    }*/

    public List<Profesion> findProfesionById(Integer id) {
        List<Profesion> lista = new ArrayList<>();

        if (!profesionRepository.existsById(id))
            throw new NotFoundException("Profesion no encontrado");

        Profesion profesion = profesionRepository.findById(id).get();
        lista.add(profesion);
        return lista;
    }

    public ProfesionDto save (ProfesionDto entity){
        entity.setId(null);
        return profesionMapper.entityToDto(profesionRepository.save(profesionMapper.dtoToEntity(entity)));
    }

    public boolean eliminar(Integer id) {
        if (!profesionRepository.existsById(id))
            throw new NotFoundException("Turno no encontrado");

        profesionRepository.deleteById(id);
        return true;
    }

    public ProfesionEnteroDto editar (Integer id, ProfesionEnteroDto dto){
        if (!profesionRepository.existsById(id))
            throw new NotFoundException("No existe la profesion");

        dto.setId(id);

        return
                profesionMapper.entityToEnteroDto(profesionRepository.<Profesion>save(profesionMapper.enteroDtoToEntity(dto)));
    }

    public Page<ProfesionEnteroDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return profesionRepository.findAll(pageable).map(profesionMapper::entityToEnteroDto);
    }
    public Page<ProfesionEnteroDto> listarUnoByPage(Integer id) {
        Pageable pageable = PageRequest.of(0,1,Sort.by("id"));
        return profesionRepository.findById(id,pageable).map(profesionMapper::entityToEnteroDto);
    }
}
