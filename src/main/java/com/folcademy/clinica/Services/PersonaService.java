package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.*;
import com.folcademy.clinica.Model.Dtos.*;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonaService /*implements ITurnoService*/ {
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;
    public int acumulador = 0;

    public PersonaService(PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    static boolean esSoloLetras(String cadena) {
        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.toUpperCase().charAt(i);
            int valorASCII = (int) caracter;
            if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
                return false;
        }
        return true;
    }

    public static boolean esSoloNumeros(String cadena) {
        return cadena.matches("[0-9]+");
    }


    public PersonaDto save (PersonaDto entity){
        return personaMapper.entityToDto(personaRepository.save(personaMapper.dtoToEntity(entity)));
    }

    public boolean eliminar(Integer dni) {
        if (!personaRepository.existsById(dni))
            throw new NotFoundException("Persona no encontrada");
        personaRepository.deleteById(dni);
        return true;
    }

    public PersonaDto editar (Integer dni, PersonaDto dto){
        dto.setDni(dni);
        //dto -> entidad;
        //guardar;
        //entidad -> dto;
        return
                personaMapper.entityToDto(
                        personaRepository.save(
                                personaMapper.dtoToEntity(dto)
                        )
                );
    }
    public Page<PersonaEnteroDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return personaRepository.findAll(pageable).map(personaMapper::entityToEnteroDto);
    }
    public Page<PersonaEnteroDto> listarUnoByPage(Integer dni) {
        Pageable pageable = PageRequest.of(0,1,Sort.by("dni"));
        return personaRepository.findByDni(dni,pageable).map(personaMapper::entityToEnteroDto);
    }
}