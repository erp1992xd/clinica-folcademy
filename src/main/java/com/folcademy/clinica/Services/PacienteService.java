package com.folcademy.clinica.Services;




import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;

import com.folcademy.clinica.Model.Dtos.ProfesionDto;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PacienteService /*implements IPacienteService*/ {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    public int acumulador = 0;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    /*public PacienteEnteroDto findPacienteById(Integer id){
        if(pacienteRepository.existsById(id))
            throw new NotFoundException("No exite paciente");
        return pacienteRepository.findById(id).map(pacienteMapper::entityToEnteroDto).orElse(null);

    }*/

    static boolean esSoloLetras(String cadena) {
        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.toUpperCase().charAt(i);
            int valorASCII = (int) caracter;
            if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
                return false;
        }
        return true;
    }

    public static boolean esSoloNumeros(String cadena) {
        return cadena.matches("[0-9]+");
    }

    public PacienteDto save (PacienteDto entity){
        entity.setId(null);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
    }
    public PacienteDto editar (Integer id, PacienteDto dto){
        if (!pacienteRepository.existsById(id))
            return null;
        dto.setId(id);
        //dto -> entidad;
        //guardar;
        //entidad -> dto;
        return
                pacienteMapper.entityToDto(
                        pacienteRepository.save(
                                pacienteMapper.dtoToEntity(dto)
                        )
                );
    }

    public boolean eliminar(Integer id) {
        if (!pacienteRepository.existsById(id))
            throw new NotFoundException("Paciente no encontrado");

        pacienteRepository.deleteById(id);
        return true;
    }

    public Page<PacienteEnteroDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize,Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToEnteroDto);
    }

    public Page<PacienteEnteroDto> listarUnoByPage(Integer id) {
        Pageable pageable = PageRequest.of(0,1,Sort.by("id"));
        return pacienteRepository.findById(id,pageable).map(pacienteMapper::entityToEnteroDto);
    }

}


