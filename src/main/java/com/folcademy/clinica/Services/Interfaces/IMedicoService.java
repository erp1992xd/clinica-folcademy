package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;

import java.util.List;

public interface IMedicoService {

    List<MedicoDto> findAllMedico();
    List<MedicoDto> findMedicoById(Integer id);
    MedicoDto save(Medico entity);



}
