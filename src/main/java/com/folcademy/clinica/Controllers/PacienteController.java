package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping("/page")
    public ResponseEntity<Page<PacienteEnteroDto>> listarTodosByPage(
            @RequestParam(name = "pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "persona.apellido") String orderField
    ){
        return ResponseEntity.ok(pacienteService.listarTodosByPage(pageNumber,pageSize,orderField));

    }
    @PreAuthorize("hasAuthority('get')")
    @GetMapping("/{id}")
    public ResponseEntity<Page<PacienteEnteroDto>> listarUno(@PathVariable(name="id")Integer id)
    {
        return ResponseEntity.ok().body(pacienteService.listarUnoByPage(id));
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<PacienteDto> save(@RequestBody @Validated PacienteDto dto) {
        PacienteMapper pacienteMapper = new PacienteMapper();
        return ResponseEntity.ok(pacienteService.save(dto));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{id}")
    public ResponseEntity<PacienteDto> editar(@PathVariable(name = "id") int id,
                                                    @RequestBody PacienteDto dto) {
        return ResponseEntity.ok().body(pacienteService.editar(id,dto));
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }
}
