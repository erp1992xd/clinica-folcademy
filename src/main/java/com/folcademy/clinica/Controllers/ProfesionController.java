package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Dtos.ProfesionDto;
import com.folcademy.clinica.Model.Dtos.ProfesionEnteroDto;
import com.folcademy.clinica.Model.Entities.Profesion;
import com.folcademy.clinica.Model.Mappers.ProfesionMapper;
import com.folcademy.clinica.Services.ProfesionService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/profesion")
public class ProfesionController {

    private final ProfesionService profesionService;

    public ProfesionController(ProfesionService profesionService) {
        this.profesionService = profesionService;
    }


    /*@GetMapping(value = "")
    public ResponseEntity<List<Profesion>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        profesionService.findAllProfesiones())
                ;
    }*/
    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<ProfesionEnteroDto>> listarTodosByPage(
            @RequestParam(name = "pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "nombre") String orderField
    ){
        return ResponseEntity.ok(profesionService.listarTodosByPage(pageNumber,pageSize,orderField));
    }
    @PreAuthorize("hasAuthority('get')")
    @GetMapping("/{id}")
    public ResponseEntity<Page<ProfesionEnteroDto>> listarUno(@PathVariable(name="id")Integer id)
    {
        return ResponseEntity.ok().body(profesionService.listarUnoByPage(id));
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<ProfesionDto> save(@RequestBody @Validated ProfesionDto dto) {
        ProfesionMapper profesionMapper = new ProfesionMapper();
        return ResponseEntity.ok(profesionService.save(dto));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{id}")
    public ResponseEntity<ProfesionEnteroDto> editar(@PathVariable(name = "id") int id,
                                                     @RequestBody ProfesionEnteroDto dto) {
        return ResponseEntity.ok().body(profesionService.editar(id,dto));
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok(profesionService.eliminar(id));
    }
}
