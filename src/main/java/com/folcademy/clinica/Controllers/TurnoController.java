package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;

import com.folcademy.clinica.Model.Entities.Turno;

import com.folcademy.clinica.Model.Mappers.TurnoMapper;

import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }


    //@GetMapping(value = "")
    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<TurnoEnteroDto>> listarTodosByPage(
            @RequestParam(name = "pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize
            //@RequestParam(name = "orderField",defaultValue = "persona.apellido") String orderField
    ){
        return ResponseEntity.ok(turnoService.listarTodosByPage(pageNumber,pageSize));
    }
    @PreAuthorize("hasAuthority('get')")
    @GetMapping("/{id}")
    public ResponseEntity<Page<TurnoEnteroDto>> listarUno(@PathVariable(name="id")Integer id)
    {
        return ResponseEntity.ok().body(turnoService.listarUnoByPage(id));
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> save(@RequestBody @Validated TurnoDto dto) {
        TurnoMapper turnoMapper = new TurnoMapper();
        return ResponseEntity.ok(turnoService.save(dto));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{id}")
    public ResponseEntity<TurnoEnteroDto> editar(@PathVariable(name = "id") int id,
                                                 @RequestBody TurnoEnteroDto dto) {
        return ResponseEntity.ok().body(turnoService.editar(id,dto));
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok(turnoService.eliminar(id));
    }

}
