package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {

    private final MedicoService medicoService;


    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<MedicoEnteroDto>> listarTodosByPage(
            @RequestParam(name = "pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "persona.apellido") String orderField
    ){
        return ResponseEntity.ok(medicoService.listarTodosByPage(pageNumber,pageSize,orderField));
    }
    @PreAuthorize("hasAuthority('get')")
    @GetMapping("/{id}")
    public ResponseEntity<Page<MedicoEnteroDto>> listarUno(@PathVariable(name="id")Integer id)
    {
        return ResponseEntity.ok().body(medicoService.listarUnoByPage(id));
    }


    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<MedicoDto> save(@RequestBody @Validated MedicoDto dto) {
        return ResponseEntity.ok(medicoService.save(dto));
    }
    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{id}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "id") int id,
                                                  @RequestBody MedicoEnteroDto dto) {
        return ResponseEntity.ok().body(medicoService.editar(id,dto));
    }
    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok(medicoService.eliminar(id));
    }

}


