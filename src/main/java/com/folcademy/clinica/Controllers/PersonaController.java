package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.*;

import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Entities.Turno;

import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;

import com.folcademy.clinica.Services.PersonaService;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/persona")
public class PersonaController {

    private final PersonaService personaService;

    public PersonaController(PersonaService personaService) {
        this.personaService = personaService;
    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<PersonaEnteroDto>> listarTodosByPage(
            @RequestParam(name = "pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "apellido") String orderField
    ){
        return ResponseEntity.ok(personaService.listarTodosByPage(pageNumber,pageSize,orderField));
    }
    @PreAuthorize("hasAuthority('get')")
    @GetMapping("/{dni}")
    public ResponseEntity<Page<PersonaEnteroDto>> listarUno(@PathVariable(name="dni")Integer dni)
    {
        return ResponseEntity.ok().body(personaService.listarUnoByPage(dni));

    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<PersonaDto> save(@RequestBody @Validated PersonaDto dto) {
        return ResponseEntity.ok(personaService.save(dto));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{id}")
    public ResponseEntity<PersonaDto> editar(@PathVariable(name = "id") int dni,
                                                   @RequestBody PersonaDto dto) {
        return ResponseEntity.ok().body(personaService.editar(dni,dto));
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok(personaService.eliminar(id));
    }
}
